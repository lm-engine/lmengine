#include <entt/entt.hpp>

#include <fmt/format.h>
#include <lmengine/physics.h>
#include <lmengine/reflection.h>
#include <lmengine/shapes.h>
#include <lmengine/transform.h>

namespace lmng
{
char const *get_data_name(entt::meta_data const &data)
{
    return data.prop("name"_hs.value()).value().cast<char const *>();
}
char const *get_type_name(entt::meta_type const &type)
{
    return type.prop("name"_hs.value()).value().cast<char const *>();
}

// Registry can't be const because then it
// wouldn't be able to return a non-const component.
entt::meta_any get_component_any(
  entt::registry &registry,
  entt::entity entity,
  entt::meta_type const &type)
{
    auto get_func = type.func("get_from_entity"_hs);
    if (!get_func)
        throw std::runtime_error{"entt meta error."};
    return get_func.invoke({}, &registry, entity);
}

// Registry can't be const because then it
// wouldn't be able to return a non-const component.
entt::meta_any get_component_any(
  entt::registry const &registry,
  entt::entity entity,
  entt::meta_type const &type)
{
    auto get_func = type.func("get_from_entity"_hs);
    if (!get_func)
        throw std::runtime_error{"entt meta error."};
    return get_func.invoke({}, &registry, entity);
}

void assign_to_entity(
  entt::meta_any const &component,
  entt::registry &registry,
  entt::entity entity)
{
    component.type()
      .func("assign_to_entity"_hs)
      .invoke({}, component.data(), &registry, entity);
}

void replace_on_entity(
  entt::meta_any const &component,
  entt::registry &registry,
  entt::entity entity)
{
    component.type()
      .func("replace_on_entity"_hs)
      .invoke({}, component.data(), &registry, entity);
}

std::string
  get_data(entt::meta_any const &component, entt::meta_data const &data)
{
    auto as_string_name = fmt::format("get_{}_as_str", get_data_name(data));
    entt::meta_func to_string =
      component.type().func(entt::hashed_string{as_string_name.c_str()});
    return to_string.invoke({}, component.data()).cast<std::string>();
}

void set_data(
  entt::meta_any &component,
  entt::meta_data const &data,
  std::string const &string)
{
    auto from_string_name = fmt::format("set_{}_from_str", get_data_name(data));
    entt::meta_func from_string =
      component.type().func(entt::hashed_string{from_string_name.c_str()});
    from_string.invoke({}, component.data(), &string);
}

void clone(
  entt::registry const &from,
  entt::registry &to,
  entt::meta_type const &type)
{
    auto meta_func = type.func("clone"_hs);
    meta_func.invoke({}, &from, &to);
}

template <>
void lmng::stream_data_out(std::ostream &stream, Eigen::Vector3f const &data)
{
    stream << fmt::format("{} {} {}", data[0], data[1], data[2]);
}

template <>
void lmng::stream_data_in(std::istream &stream, Eigen::Vector3f &data)
{
    stream >> data[0] >> data[1] >> data[2];
}

template <>
void lmng::stream_data_out(std::ostream &stream, Eigen::Quaternionf const &data)
{
    stream << fmt::format(
      "{} {} {} {}", data.x(), data.y(), data.z(), data.w());
}

template <>
void lmng::stream_data_in(std::istream &stream, Eigen::Quaternionf &data)
{
    stream >> data.x() >> data.y() >> data.z() >> data.w();
}

void stream_data_out(std::ostream &stream, std::array<float, 3> const &data)
{
    stream << fmt::format("{} {} {}", data[0], data[1], data[2]);
}

void stream_data_in(std::istream &stream, std::array<float, 3> &data)
{
    for (auto &datum : data)
        stream >> datum;
}

void set_meta_context(entt::meta_ctx const &ctx) { entt::meta_ctx::bind(ctx); }

void reflect_types()
{
    entt::meta<uint32_t>().type();
    entt::meta<char const *>().type();
    entt::meta<entt::registry *>().type();
    entt::meta<entt::entity>().type();
    entt::meta<void const *>().type();
    entt::meta<void *>().type();
    entt::meta<std::string>().type();
    entt::meta<std::string const *>().type();

    REFLECT_TYPE(box_render, "Box Render")
      .REFLECT_MEMBER(box_render, extents, "Extents")
      .REFLECT_MEMBER(box_render, colour, "Colour");

    REFLECT_TYPE(transform, "Transform")
      .REFLECT_MEMBER(transform, position, "Position")
      .REFLECT_MEMBER(transform, rotation, "Rotation");

    REFLECT_TYPE(rigid_body, "Rigid Body")
      .REFLECT_MEMBER(rigid_body, mass, "Mass")
      .REFLECT_MEMBER(rigid_body, restitution, "Restitution")
      .REFLECT_MEMBER(rigid_body, friction, "Friction");
}
} // namespace lmng
