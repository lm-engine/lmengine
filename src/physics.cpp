#include <btBulletDynamicsCommon.h>
#include <range/v3/algorithm/find.hpp>

#include <lmengine/physics.h>
#include <lmengine/shapes.h>
#include <lmengine/transform.h>

#include "physics_util.h"

namespace lmng
{
struct bt_rigid_body
{
    std::unique_ptr<btCollisionShape> shape;
    std::unique_ptr<btMotionState> motion_state;
    std::unique_ptr<btRigidBody> rigid_body;
};

class bt_physics : public iphysics
{
  public:
    explicit bt_physics(entt::registry &registry);
    void step(entt::registry &entities, float dt) override;
    bt_rigid_body create_box_rigid_body(
      entt::entity entity,
      const box_render &box_shape,
      const transform &transform,
      rigid_body const &abstract_rigid_body);
    btDynamicsWorld *operator->() { return physics_world.get(); }
    void set_gravity(Eigen::Vector3f const &vector) override;
    void apply_impulse(
      entt::registry &registry,
      entt::entity entity,
      Eigen::Vector3f const &vector) override;
    void apply_force(
      entt::registry &registry,
      entt::entity entity,
      Eigen::Vector3f const &vector) override;
    void on_tick(btDynamicsWorld *world, btScalar dt);
    bool is_touched(entt::registry &registry, entt::entity entity) override;

  private:
    std::unique_ptr<btBroadphaseInterface> broadphase;
    std::unique_ptr<btCollisionConfiguration> collision_configuration;
    std::unique_ptr<btDispatcher> dispatcher;
    std::unique_ptr<btConstraintSolver> solver;
    std::unique_ptr<btDiscreteDynamicsWorld> physics_world;
};

void world_tick_callback(btDynamicsWorld *world, btScalar dt)
{
    auto bt_physics_instance =
      static_cast<bt_physics *>(world->getWorldUserInfo());

    bt_physics_instance->on_tick(world, dt);
}

bt_physics::bt_physics(entt::registry &registry)
{
    broadphase = std::make_unique<btDbvtBroadphase>();

    collision_configuration =
      std::make_unique<btDefaultCollisionConfiguration>();

    dispatcher =
      std::make_unique<btCollisionDispatcher>(collision_configuration.get());

    solver = std::make_unique<btSequentialImpulseConstraintSolver>();

    physics_world = std::make_unique<btDiscreteDynamicsWorld>(
      dispatcher.get(),
      broadphase.get(),
      solver.get(),
      collision_configuration.get());

    physics_world->setInternalTickCallback(world_tick_callback);

    registry.view<transform, rigid_body, box_render>().each(
      [&](
        auto entity,
                                                       auto const &transform,
                                                       auto const &rigid_body,
                                                       auto const &box_shape) {
        registry.assign<bt_rigid_body>(
          entity,
          create_box_rigid_body(entity, box_shape, transform, rigid_body));
    });
}

void bt_physics::step(entt::registry &entities, float dt)
{
    physics_world->setWorldUserInfo(this);
    physics_world->stepSimulation(dt, 10, 1.f / 144.f);
    physics_world->setWorldUserInfo(nullptr);

    entities.view<transform, bt_rigid_body>().each(
      [&](auto entity, transform &transform, bt_rigid_body &rigid_body) {
          auto bt_transform = rigid_body.rigid_body->getWorldTransform();
          auto origin = bt_transform.getOrigin();
          transform.position[0] = origin.getX();
          transform.position[1] = origin.getY();
          transform.position[2] = origin.getZ();

          auto rotation = bt_transform.getRotation();
          transform.rotation = Eigen::Quaternionf{
            rotation.getW(), rotation.getX(), rotation.getY(), rotation.getZ()};
      });
}

bt_rigid_body bt_physics::create_box_rigid_body(
  entt::entity entity,
  const box_render &box_shape,
  const transform &transform,
  rigid_body const &abstract_rigid_body)
{
    auto box = std::make_unique<btBoxShape>(vec_to_bt(box_shape.extents));

    auto box_motion_state = std::make_unique<btDefaultMotionState>(btTransform{
      quat_to_bt(transform.rotation),
      vec_to_bt(transform.position),
    });

    btVector3 inertia;

    box->calculateLocalInertia(abstract_rigid_body.mass, inertia);

    auto box_body =
      std::make_unique<btRigidBody>(btRigidBody::btRigidBodyConstructionInfo(
        abstract_rigid_body.mass, box_motion_state.get(), box.get(), inertia));

    box_body->setRestitution(abstract_rigid_body.restitution);
    box_body->setFriction(abstract_rigid_body.friction);
    box_body->setUserIndex(int(entity));

    physics_world->addRigidBody(box_body.get());

    return bt_rigid_body{
      .shape = std::move(box),
      .motion_state = std::move(box_motion_state),
      .rigid_body = std::move(box_body),
    };
}

void bt_physics::set_gravity(Eigen::Vector3f const &vector)
{
    physics_world->setGravity({vector[0], vector[1], vector[2]});
}

void bt_physics::apply_impulse(
  entt::registry &registry,
  entt::entity entity,
  Eigen::Vector3f const &vector)
{
    bt_rigid_body &rigid_body = registry.get<bt_rigid_body>(entity);
    rigid_body.rigid_body->activate();
    rigid_body.rigid_body->applyCentralImpulse(vec_to_bt(vector));
}

void bt_physics::apply_force(
  entt::registry &registry,
  entt::entity entity,
  Eigen::Vector3f const &vector)
{
    bt_rigid_body &rigid_body = registry.get<bt_rigid_body>(entity);
    rigid_body.rigid_body->activate();
    rigid_body.rigid_body->applyCentralForce(vec_to_bt(vector));
}

void bt_physics::on_tick(btDynamicsWorld *world, btScalar dt) {}

bool bt_physics::is_touched(entt::registry &registry, entt::entity entity)
{
    for (auto &manifold : manifolds(physics_world.get()))
    {
        std::array entities{
          manifold.getBody0()->getUserIndex(),
          manifold.getBody1()->getUserIndex(),
        };
        if (ranges::find(entities, int(entity)) != entities.end())
        {
            if (manifold.getNumContacts() >= 1)
            {
                return true;
            }
        }
    }
    return false;
}

std::unique_ptr<iphysics> create_physics(entt::registry &registry)
{
    return std::make_unique<bt_physics>(registry);
}
} // namespace lmng
