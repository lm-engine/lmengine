#pragma once

#include <LinearMath/btVector3.h>

btVector3 vec_to_bt(Eigen::Vector3f const &vec)
{
    return btVector3{vec[0], vec[1], vec[2]};
}

btQuaternion quat_to_bt(Eigen::Quaternionf const &quat)
{
    return btQuaternion{quat.x(), quat.y(), quat.z(), quat.w()};
}

class manifolds
{
  public:
    explicit manifolds(btDynamicsWorld *physics_world)
        : dispatcher{physics_world->getDispatcher()},
          n_manifolds{dispatcher->getNumManifolds()}
    {
    }

    struct iterator
    {
        using index_type = int;
        using reference = btPersistentManifold &;

        bool operator!=(iterator const &other) { return index != other.index; }

        reference operator*()
        {
            return *dispatcher->getManifoldByIndexInternal(index);
        }

        iterator &operator++()
        {
            index++;
            return *this;
        }

        btDispatcher *dispatcher;
        int index;
    };

    iterator begin() { return iterator{dispatcher, 0}; }

    iterator end() { return iterator{dispatcher, n_manifolds}; }

    btDispatcher *dispatcher;
    int n_manifolds;
};
