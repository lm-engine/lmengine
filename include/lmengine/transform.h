#pragma once

#include <lmlib/geometry.h>

#include "named_type.h"

namespace lmng
{
struct transform
{
    Eigen::Vector3f position{Eigen::Vector3f::Zero()};
    Eigen::Quaternionf rotation{Eigen::Quaternionf::Identity()};
};
} // namespace lmng

LMNG_NAMED_TYPE(lmng::transform, "Transform");
