from conans import ConanFile, CMake, tools


class LmengineConan(ConanFile):
    name = "lmengine"
    version = "0.0.1"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake_find_package"
    requires = (
        'lmlib/0.0.1',
        'cereal/1.2.2@conan/stable',
        'entt/3.2.0@skypjack/stable',
        'bullet3/2.87@bincrafters/stable',
        'yaml-cpp/0.6.2@bincrafters/stable',
    )

    def imports(self):
        self.copy('*.dll', src='bin')

    def package_info(self):
        self.cpp_info.libs = ['lmengine']
